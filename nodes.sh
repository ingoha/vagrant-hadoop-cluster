#!/bin/sh

source /vagrant/common.sh

: "Set environment variables to shell RC file" && {
  grep JAVA_HOME /etc/hosts > /dev/null
  if [ $? -ne 0 ]; then
    cat << EOF >> ~/.bashrc
export JAVA_HOME=/usr/lib/jvm/jre-1.8.0-openjdk
export HADOOP_HOME=~/hadoop-$HADOOP_VER
export HADOOP_CONF_DIR=\$HADOOP_HOME/etc/hadoop
export PATH=\$HADOOP_HOME/bin:\$HADOOP_HOME/sbin:\$JAVA_HOME/bin:\$PATH
EOF
  fi
  source ~/.bashrc
}
