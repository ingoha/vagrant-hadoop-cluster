#!/bin/sh

source /vagrant/config.sh

set -x

HOSTNAME=$1

: "Set hostname" && {
  sudo hostname $HOSTNAME
  echo $HOSTNAME | sudo tee /etc/hostname > /dev/null
}

: "Edit hosts file" && {
  grep 192.168.33.10 /etc/hosts > /dev/null
  if [ $? -ne 0 ] ; then
    cat << 'EOF' | sudo tee -a /etc/hosts > /dev/null
192.168.33.10 master
192.168.33.11 node1
192.168.33.12 node2
192.168.33.13 node3
192.168.33.14 node4
EOF
  fi
}

: "Enable EPEL repo" && {
  sudo dnf -y install epel-release
}

: "Upgrade system" && {
  sudo dnf -y upgrade
}

: "Install common packages" && {
  sudo dnf -y install java-1.8.0-openjdk-devel openssh-clients rsync wget sshpass nano net-tools
}

# cf. https://linuxhint.com/disable_ipv6_centos/
: "Disable IPv6" && {
  sudo sysctl net.ipv6.conf.all.disable_ipv6=1
  sudo sysctl net.ipv6.conf.default.disable_ipv6=1
}

: "Enable SSH password authentication" && {
  sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
  sudo systemctl restart sshd.service
}

: "Set environment variables to shell RC file" && {
  grep JAVA_HOME /etc/hosts > /dev/null
  if [ $? -ne 0 ]; then
    cat << EOF >> ~/.bashrc
export JAVA_HOME=/usr/lib/jvm/jre-1.8.0-openjdk
export HADOOP_HOME=~/hadoop-$HADOOP_VER
export HADOOP_CONF_DIR=\$HADOOP_HOME/etc/hadoop
export PATH=\$HADOOP_HOME/bin:\$HADOOP_HOME/sbin:\$JAVA_HOME/bin:\$PATH
EOF
  fi
  source ~/.bashrc
}

: "Linux Logo ;-)" && {
  $WGET https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/l/linux_logo-5.11-7.el7.x86_64.rpm
  sudo rpm -i linux_logo-5.11-7.el7.x86_64.rpm
  echo "linux_logo -L 26" >> .bash_profile
}
