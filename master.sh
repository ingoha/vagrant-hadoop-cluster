#!/bin/sh

source /vagrant/common.sh

: "Download Hadoop" && {
  ls | grep hadoop-*.tar.gz > /dev/null
  if [ $? -ne 0 ]; then
    $WGET https://dlcdn.apache.org/hadoop/common/hadoop-$HADOOP_VER/hadoop-$HADOOP_VER.tar.gz
    tar xf hadoop-$HADOOP_VER.tar.gz
  fi
}

: "Hadoop execution check" && {
  hadoop version
}

: "Disable SSH strict host key check" && {
  cat << 'EOF' > .ssh/config
Host *
  StrictHostKeyChecking no
EOF
  chmod 600 .ssh/config
}

: "Install SSH public key to all nodes" && {
  ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa
  for node in master node1 node2; do
    sshpass -p "vagrant" ssh-copy-id -i ~/.ssh/id_rsa.pub $node
  done;
}

: "Copy Hadoop directory to nodes" && {
    for node in node1 node2; do
      rsync -a $HADOOP_HOME $node:~/
    done;
}

: "Setting configuration files" && {

  : "etc/hadoop/slaves" && {
    cat << 'EOF' > $HADOOP_HOME/etc/hadoop/slaves
node1
node2
EOF
  }

  : "etc/hadoop/core-site.xml" && {
    grep fs.defaultFS $HADOOP_HOME/etc/hadoop/core-site.xml
    if [ $? -ne 0 ]; then
      cat << 'EOF' > /tmp/core-site.xml.property
  <property>
    <name>fs.defaultFS</name>
    <value>hdfs://master:9000</value>
  </property>
  <property>
    <name>hadoop.http.staticuser.user</name>
    <value>vagrant</value>
  </property>
EOF
      sed -i -e '
        /^<configuration>$/r /tmp/core-site.xml.property
        /^$/d
      ' $HADOOP_HOME/etc/hadoop/core-site.xml
    fi
  }

  : "etc/hadoop/hdfs-site.xml" && {
    grep dfs.replication $HADOOP_HOME/etc/hadoop/hdfs-site.xml
    if [ $? -ne 0 ]; then
      cat << 'EOF' > /tmp/hdfs-site.xml.property
  <property>
    <name>dfs.replication</name>
    <value>2</value>
  </property>
  <property>
    <name>dfs.namenode.secondary.http-address</name>
    <value>master:50090</value>
  </property>
EOF
      sed -i -e '
        /^<configuration>$/r /tmp/hdfs-site.xml.property
        /^$/d
      ' $HADOOP_HOME/etc/hadoop/hdfs-site.xml
    fi
  }

  : "etc/hadoop/mapred-site.xml" && {
    grep mapreduce.framework.nam $HADOOP_HOME/etc/hadoop/mapred-site.xml
    if [ $? -ne 0 ]; then
      cp $HADOOP_HOME/etc/hadoop/mapred-site.xml{.template,}
      cat << 'EOF' > /tmp/mapred-site.xml.property
  <property>
    <name>mapreduce.framework.name</name>
    <value>yarn</value>
  </property>
EOF
      sed -i -e '
        /^<configuration>$/r /tmp/mapred-site.xml.property
        /^$/d
      ' $HADOOP_HOME/etc/hadoop/mapred-site.xml
    fi
  }

  : "etc/hadoop/yarn-site.xml" && {
    grep yarn.nodemanager.aux-service $HADOOP_HOME/etc/hadoop/yarn-site.xml
    if [ $? -ne 0 ]; then
      cat << 'EOF' > /tmp/yarn-site.xml.property
  <property>
    <name>yarn.nodemanager.aux-services</name>
    <value>mapreduce_shuffle</value>
  </property>
  <property>
    <name>yarn.resourcemanager.hostname</name>
    <value>master</value>
  </property>
EOF
      sed -i -e '
        /^<configuration>$/r /tmp/yarn-site.xml.property
        /^$/d
      ' $HADOOP_HOME/etc/hadoop/yarn-site.xml
    fi
  }

  : "Copy to slaves" && {
    for node in node1 node2; do
      rsync -a $HADOOP_HOME/etc/hadoop/* $node:$HADOOP_HOME/etc/hadoop/
    done;
  }

}

: "Format HDFS" && {
  $HADOOP_HOME/bin/hdfs namenode -format -force
}

: "Start daemons" && {

  : "HDFS" && {
    jps | grep NameNode
    if [ $? -ne 0 ]; then
      $HADOOP_HOME/sbin/start-dfs.sh
    fi
  }

  : "YARN" && {
    jps | grep ResourceManager
    if [ $? -ne 0 ]; then
      $HADOOP_HOME/sbin/start-yarn.sh
    fi
  }

  : "MapReduce JobHistory server" && {
    jps | grep JobHistoryServer
    if [ $? -ne 0 ]; then
      $HADOOP_HOME/sbin/mr-jobhistory-daemon.sh --config $HADOOP_CONF_DIR start historyserver
    fi
  }
}

: "Setup Hive" && {

  : "Download Hive (2.x)" && {
    ls | grep apache-hive-*.tar.gz > /dev/null
    if [ $? -ne 0 ]; then
      $WGET https://dlcdn.apache.org/hive/hive-$HIVE_VER/apache-hive-$HIVE_VER-bin.tar.gz -nv
      tar xf apache-hive-$HIVE_VER-bin.tar.gz
    fi
  }

  : "Set environment variables to shell RC file" && {
    grep HIVE_HOME ~/.bashrc > /dev/null
    if [ $? -ne 0 ]; then
      cat << EOF >> ~/.bashrc
export HIVE_HOME=~/apache-hive-$HIVE_VER-bin
export PATH=\$HIVE_HOME/bin:\$PATH
EOF
    fi
    source ~/.bashrc
}

  : "Setup HDFS working directory" && {
    $HADOOP_HOME/bin/hadoop fs -mkdir -p /user/hive/warehouse
    $HADOOP_HOME/bin/hadoop fs -chmod g+w /user/hive/warehouse
    $HADOOP_HOME/bin/hadoop fs -mkdir -p /tmp
    $HADOOP_HOME/bin/hadoop fs -chmod g+w /tmp
    $HIVE_HOME/bin/schematool -dbType derby -initSchema --verbose
  }

}

# cf. https://hbase.apache.org/book.html#quickstart_pseudo
: "Setup HBase" && {

  : "Download HBase" && {
   ls | grep hbase-*.tar.gz > /dev/null
   if [ $? -ne 0 ]; then
     $WGET https://archive.apache.org/dist/hbase/$HBASE_VER/hbase-$HBASE_VER-bin.tar.gz
     tar xf hbase-$HBASE_VER-bin.tar.gz
   fi
  }

  : "Set environment variables to shell RC file" && {
    grep HBASE_HOME ~/.bashrc > /dev/null
    if [ $? -ne 0 ]; then
      cat << EOF >> ~/.bashrc
export HBASE_HOME=~/hbase-${HBASE_VER}
export PATH=\$HBASE_HOME/bin:\$PATH
EOF
    fi
    source ~/.bashrc
  }

  : "Setting configuration files" && {

  : "conf/hbase-site.xml" && {
  cat << 'EOF' > $HBASE_HOME/conf/hbase-site.xml
<configuration>
  <property>
    <name>hbase.cluster.distributed</name>
    <value>true</value>
  </property>
  <property>
    <name>hbase.rootdir</name>
    <value>hdfs://master:9000/hbase</value>
  </property>
  <property>
    <name>hbase.zookeeper.quorum</name>
    <value>master,node1,node2</value>
</property>
</configuration>
EOF
  }

  : "conf/regionservers" && {
  cat << 'EOF' > $HBASE_HOME/conf/regionservers
node1
node2
EOF
  }

  : "conf/backup-masters" && {
  cat << 'EOF' > $HBASE_HOME/conf/backup-masters
node1
EOF
  }
  }
}

: "Copy HBase to nodes" && {
  for node in node1 node2; do
    rsync -a $HBASE_HOME $node:~
  done;
}

: "Start HBase" && {

  jps | grep HMaster
  if [ $? -ne 0 ]; then
    $HBASE_HOME/bin/start-hbase.sh
  fi
}

: "Create start script" && {
  cat << 'EOF' > $HOME/start-cluster.sh
    jps | grep NameNode
    if [ $? -ne 0 ]; then
      $HADOOP_HOME/sbin/start-dfs.sh
    fi

    jps | grep ResourceManager
    if [ $? -ne 0 ]; then
      $HADOOP_HOME/sbin/start-yarn.sh
    fi

    jps | grep JobHistoryServer
    if [ $? -ne 0 ]; then
      $HADOOP_HOME/sbin/mr-jobhistory-daemon.sh --config $HADOOP_CONF_DIR start historyserver
    fi

  jps | grep HMaster
  if [ $? -ne 0 ]; then
    $HBASE_HOME/bin/start-hbase.sh
  fi
EOF
  chmod +x $HOME/start-cluster.sh
}
