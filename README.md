Run `vagrant up

# Introduction
Installs a Hadoop/HBase cluster using three VMs:
* Hadoop is installed with one master and two slave nodes
* HBase is installed in [fully distributed mode](https://hbase.apache.org/book.html#quickstart_fully_distributed)

## HBase cluster architecture

| **Node Name**         | **Master**     | **Zookeeper** | **RegionServer** |
|--------------|-----------|------------|------------|
| master | yes      | yes        | no |
| node1  | backup      | yes        | yes |
| node2  | no      | yes        | yes |
# Security
There is (nearly) none ;-) This is a dev setup, not meant for production.

Everything is run under the user vagrant.

# Ports

The VMs are placed in the (host-only) network 192.168.33.0/24 and should be reachable at their addresses (master=10, node1=11, node2=12).

* Hadoop: 8088, 50070
* HBase: 16010

# Links
* based on https://gist.github.com/momijiame/0ff814ce4c3aa659723c6b5b0fc85557 
